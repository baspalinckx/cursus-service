package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Attitude;
import nl.kzaconnected.cursus.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttitudeService {

    @Autowired
    private AttitudeRepository attitudeRepository;

    public List<Attitude> findAll(){
        return attitudeRepository.findAll();
    }
}
