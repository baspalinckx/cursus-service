package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Functieniveau;
import nl.kzaconnected.cursus.repository.FunctieniveauRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FunctieniveauService {

    @Autowired
    private FunctieniveauRepository functieniveauRepository;

    public List<Functieniveau> findAll(){
        return functieniveauRepository.findAll();
    }
}
